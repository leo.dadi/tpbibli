package com.example.java_bibli;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.io.IOException;

public class UserController {

    public User user;

    Stage stage;
    Scene scene;
    Parent root;
    @FXML
    Label welcomeText;
    @FXML
    MenuBar menuBar;
    @FXML
    Menu menuEmprunts;
    @FXML
    MenuItem empruntsEnCours,empruntsTermine,empruntsTous;
    @FXML
    MenuItem tousLivres,livresDisponibles,livresEmpruntes,listeUtilisateurs;
    @FXML
    Menu adminMenu;
    public void setWelcomeText(String name){
        welcomeText.setText("Bienvenue " + name);
    }
    public void setUser(User user){
        this.user= user;
    }

    public void showAdmin(User user){
        if(user.getCategorie()!=1){
            adminMenu.setVisible(false);
        }else{adminMenu.setVisible(true);}
    }
    @FXML
    public void emprunts(ActionEvent event) {
        try {
            stage = (Stage) menuBar.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("emprunts.fxml"));
            root = loader.load();

            EmpruntsController controller2 = loader.getController();
            controller2.setUser(user);

            if(event.getSource()==empruntsEnCours){controller2.buildData(0);}
            else if(event.getSource()==empruntsTermine){
                controller2.buildData(1);}
            else{
                controller2.buildData(3);
            }

            scene = new Scene(root);
            stage.setScene(scene);
            //stage.show();
            //stage.centerOnScreen();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    @FXML
    public void emprunter(){
        try {
            stage = (Stage) menuBar.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("emprunter.fxml"));
            root = loader.load();

            EmprunterController controller2 = loader.getController();
            controller2.setUser(user);
            controller2.montrerLivres();
            scene = new Scene(root);
            stage.setScene(scene);

        }catch (Exception e){System.out.println();}
    }
    @FXML
    public void liste(ActionEvent event) throws IOException {
        stage = (Stage) menuBar.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("recherche.fxml"));
        root = loader.load();
        RechercheController controller2=loader.getController();
        controller2.setUser(user);
        MenuItem source = (MenuItem) event.getSource();
        if (source==tousLivres) {
            controller2.buildData(0);
        }else if(source==livresDisponibles){
            controller2.buildData(1);
        } else if (source==livresEmpruntes) {
            controller2.buildData(2);
        }else {
            controller2.buildData(3);
        }
        scene = new Scene(root, 960,720);
        stage.setScene(scene);
        stage.show();
        stage.centerOnScreen();

    }
    @FXML
    public void gotoAdmin() throws IOException {
        if (user.getCategorie() == 1) {
            stage = (Stage) menuBar.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("adminMenu.fxml"));
            root = loader.load();
            AdminMenuController controller2 = loader.getController();
            controller2.setUser(user);
            controller2.buildData();
            scene = new Scene(root, 960, 720);
            stage.setScene(scene);
            stage.show();
            stage.centerOnScreen();

        }
    }
    @FXML
    public void deconnexion(ActionEvent event) throws IOException {

        stage = (Stage) menuBar.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        root = loader.load();
        scene = new Scene(root, 960,720);
        stage.setScene(scene);
        stage.show();
        stage.centerOnScreen();
        System.out.println("Deconnexion");
    }
}
