package com.example.java_bibli;

import javafx.beans.property.*;
import javafx.collections.ObservableArray;
import javafx.scene.control.Button;

import java.sql.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class User {

    private String firstname, lastname;
    private StringProperty login;
    private int categorie;
    private int nbEmprunts;
    public Button red,green;
    private BooleanProperty redListed;
    public User() {
    }

    public int getCategorie(){
        return categorie;
    }

    public User(String firstname, String lastame, int catégorie, String login) {
        this.firstname = firstname;
        this.lastname = lastame;
        this.categorie = categorie;
        this.login = new SimpleStringProperty(login);

    }

    public User(String login) throws Exception {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque", "root", "mypassword");
            System.out.println("Creation du user à partir du couple login mot de passe correspondant");
            String sql = "select * from USER WHERE login = ?";
            PreparedStatement prep_stmt = con.prepareStatement(sql);
            String string = String.format("%s", login);
            prep_stmt.setString(1, string);
            ResultSet rs = prep_stmt.executeQuery();
            if (rs.next()) {
                String firstname = rs.getString(3), lastname = rs.getString(4);
                int categorie = rs.getInt(5);
                this.firstname = firstname;
                this.lastname = lastname;
                this.categorie = categorie;
                this.login = new SimpleStringProperty(login);
                this.nbEmprunts=getNbEmprunts();
                this.redListed=new SimpleBooleanProperty(isRedListed());
                this.red= new Button("Mettre en RedList");
                this.green=new Button("Enlever de la RedList");
                if(isRedListed()){
                    red.setVisible(false);
                }else{
                    green.setVisible(false);
                }
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public String getLogin() {
        return login.get();
    }

    public String getFirstname() {
        return firstname;
    }

    public Button getRed(){
        return red;
    }
    public Button getGreen(){
        return green;
    }

    public Boolean getredListed(){
        return redListed.get();
    }

    public int getMaxEmprunts() {
        int maxemprunts = 0;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque", "root", "mypassword");
            String sql = "select * from categorie WHERE categorie = ?";
            PreparedStatement prep_stmt = con.prepareStatement(sql);
            prep_stmt.setInt(1, categorie);
            ResultSet rs = prep_stmt.executeQuery();
            rs.next();
            maxemprunts=rs.getInt(2 );
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }return maxemprunts;
    }

    public void initNbEmprunts() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque", "root", "mypassword");
            String sql = "select COUNT(*) from emprunts WHERE userlogin = ? and fin is null ";
            PreparedStatement prep_stmt = con.prepareStatement(sql);
            String string = String.format("%s", login);
            prep_stmt.setString(1, string);
            ResultSet rs = prep_stmt.executeQuery();
            rs.next();
            nbEmprunts=rs.getInt(2 );
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public int getNbEmprunts(){
        return nbEmprunts;
    }
    public void rendre(){
        nbEmprunts-=1;
    }
    public void emprunter(){
        nbEmprunts+=1;
    }

    public boolean isRedListed(){
        int count = 0;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque", "root", "mypassword");
            String sql = "select COUNT(*) from REDLIST WHERE userlogin = ? and fin is null ";
            PreparedStatement prep_stmt = con.prepareStatement(sql);
            String string = String.format("%s", getLogin());
            prep_stmt.setString(1, string);
            ResultSet rs = prep_stmt.executeQuery();
            rs.next();
            count=rs.getInt(1);
        }catch (Exception e){System.out.println(e);}
        return count>=1;
    }

    public void redListed() {
        this.red.setVisible(false);
        this.green.setVisible(true);
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque", "root", "mypassword");
            String sql = "INSERT INTO REDLIST VALUES(?,?,null)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, this.getLogin());
            String string = String.format("%s", this.getLogin());
            String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            stmt.setString(2, date);
            stmt.execute();
            System.out.println("User redlisté");
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void greenListed() {
        this.red.setVisible(true);
        this.green.setVisible(false);
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque", "root", "mypassword");
            String sql = "UPDATE REDLIST SET fin=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            String string = String.format("%s", this.getLogin());
            String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            stmt.setString(1, date);
            stmt.execute();
            System.out.println("User Green listé");
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }


}


