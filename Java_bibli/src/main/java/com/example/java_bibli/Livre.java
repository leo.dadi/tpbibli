package com.example.java_bibli;

import javafx.beans.property.*;
import javafx.scene.control.Button;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;

public class Livre {

    private StringProperty titre;
    private StringProperty editeur;
    public IntegerProperty id, oeuvre_id;
    public StringProperty edition;
    public BooleanProperty disponible;
    public StringProperty auteurs;
    public Button emprunter;
    public Button rendre;

    public Livre(String titre, int id, boolean disponible, String edition){

        this.titre=new SimpleStringProperty(titre);
        this.id=new SimpleIntegerProperty(id);
        this.edition=new SimpleStringProperty(edition);
        this.disponible=new SimpleBooleanProperty(disponible);
        this.emprunter=new Button();
        emprunter.setText("Emprunter");
        this.rendre=new Button();
        rendre.setText("Rendre");
        if(disponible){
            rendre.setVisible(false);
        }else{
            emprunter.setVisible(false);
        }
    }

    public String getTitre(){
        return titre.get();
    }
    public void setTitre(String string){
        titre.set(string);
    }
    public Integer getId(){
        return id.get();
    }
    public void getId(int newId){
        id.set(newId);
    }
    public String getEdition(){
        return edition.get();
    }

    public Button getEmprunter(){
        return emprunter;
    }

    public Button getRendre(){
        return rendre;
    }
    public void emprunterAction(User user) {
        System.out.println(user.isRedListed());
        if (user.getNbEmprunts() < user.getMaxEmprunts() && !user.isRedListed()) {
            this.emprunter.setVisible(false);
            this.rendre.setVisible(true);
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                Connection con = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/Bibliotheque", "root", "mypassword");
                String sql = "INSERT INTO EMPRUNTS VALUES(?,?,?,null)";
                PreparedStatement stmt = con.prepareStatement(sql);
                stmt.setString(1, user.getLogin());
                String string = String.format("%s", user.getLogin());
                stmt.setInt(2, getId());
                String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                stmt.setString(3, date);
                stmt.execute();
                System.out.println("livre emprunté");
                sql = "UPDATE LIVRE SET disponible=False WHERE id=?";
                stmt = con.prepareStatement(sql);
                stmt.setInt(1, getId());
                stmt.execute();
                con.close();
                user.emprunter();
            } catch (Exception e) {
                System.out.println(e);
            }
        }else{
            if(user.getNbEmprunts() >= user.getMaxEmprunts()) {
                System.out.println("Nombre maximum d'emprunts réalisés");
            }else{
                    System.out.println("Vous êtes actuellemnt interdit d'emprunt");
                }

        }
    }

    public void rendreAction(User user){
        this.emprunter.setVisible(true);
        this.rendre.setVisible(false);
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque", "root", "mypassword");
            String sql="UPDATE EMPRUNTS SET fin= ? WHERE userlogin=? AND livreid=? AND fin is null";
            PreparedStatement stmt=con.prepareStatement(sql);
            String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            String string = String.format("%s", date);
            stmt.setString(1,string);
            stmt.setString(2,user.getLogin());
            stmt.setInt(3,getId());
            stmt.execute();
            sql="UPDATE LIVRE SET disponible=True WHERE id=?";
            stmt=con.prepareStatement(sql);
            stmt.setInt(1,getId());
            stmt.execute();
            con.close();
            user.rendre();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}



