package com.example.java_bibli;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class EmprunterController {

    public User user;
    public Stage stage;
    public Scene scene;
    Parent root;
    @FXML
    TableView table;
    @FXML
    MenuBar menuBar;
    @FXML
    MenuItem empruntsEnCours,empruntsTermine,empruntsTous;

    public void setUser(User user){
        this.user=user;
    }
    @FXML
    public void emprunts(ActionEvent event) {
        try {
            stage = (Stage) menuBar.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("emprunts.fxml"));
            root = loader.load();

            EmpruntsController controller2 = loader.getController();
            controller2.setUser(user);

            if(event.getSource()==empruntsEnCours){controller2.buildData(0);}
            else if(event.getSource()==empruntsTermine){
                controller2.buildData(1);}
            else{
                controller2.buildData(3);
            }
            scene = new Scene(root);
            stage.setScene(scene);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void montrerLivres() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque", "root", "mypassword");
            String sql = "SELECT livre.id, livre.titre, edition.editeur, disponible FROM livre " +
                    "JOIN edition ON livre.id=edition.bookid " +
                    "JOIN oeuvre ON oeuvre.id = livre.oeuvreid "+
                    "WHERE livre.disponible=true";
            String sql2= "SELECT livre.id, livre.titre, edition.annee, disponible FROM livre " +
                    "JOIN edition ON livre.id=edition.bookid " +
                    "JOIN oeuvre ON oeuvre.id = livre.oeuvreid "+
                    "JOIN emprunts ON emprunts.livreid=livre.id "+
                    "JOIN user ON emprunts.userlogin=user.login "+
                    "WHERE login= ? AND emprunts.fin is null";
            PreparedStatement stmt = con.prepareStatement(sql);
            PreparedStatement stmt2 = con.prepareStatement(sql2);
            String string = String.format("%s", user.getLogin());
            stmt2.setString(1,string);
            ResultSet rs = stmt.executeQuery();
            ResultSet rs2 =stmt2.executeQuery();

            ObservableList<Livre> livres = FXCollections.observableArrayList();
            double width=0.2;
            TableColumn<Livre, IntegerProperty> col1 = new TableColumn<>("ID");
            col1.setCellValueFactory(new PropertyValueFactory<>("id"));
            col1.prefWidthProperty().bind(table.widthProperty().multiply(width));
            TableColumn<Livre, StringProperty> col2 = new TableColumn<>("Titre");
            col2.setCellValueFactory(new PropertyValueFactory<>("titre"));
            col2.prefWidthProperty().bind(table.widthProperty().multiply(width));
            TableColumn<Livre, StringProperty> col3 = new TableColumn<>("Edition");
            col3.setCellValueFactory(new PropertyValueFactory<>("edition"));
            col3.prefWidthProperty().bind(table.widthProperty().multiply(width));
            TableColumn<Livre, Button> col4 = new TableColumn<>("Emprunter");
            col4.setCellValueFactory(new PropertyValueFactory("emprunter"));
            col4.prefWidthProperty().bind(table.widthProperty().multiply(width));
            TableColumn<Livre, Button> col5 = new TableColumn<>("Rendre");
            col5.setCellValueFactory(new PropertyValueFactory<>("rendre"));
            col5.prefWidthProperty().bind(table.widthProperty().multiply(width));

            table.getColumns().addAll(col1,col2,col3,col4,col5);

            while (rs.next()){
                int id=rs.getInt(1);
                String titre = rs.getString(2);
                String edition = rs.getString(3);
                boolean disponible= rs.getBoolean(4);
                livres.add( new Livre(titre, id, disponible,edition ));
            }
            while (rs2.next()){
                int id=rs2.getInt(1);
                String titre = rs2.getString(2);
                String edition = rs2.getString(3);
                boolean disponible= rs2.getBoolean(4);
                livres.add( new Livre(titre, id, disponible,edition ));
            }
            for(Livre l:livres){
                l.emprunter.setOnAction( e-> l.emprunterAction(user));
                l.rendre.setOnAction(e-> l.rendreAction(user));
            }

        table.setItems(livres);
        } catch (Exception e) {
            System.out.println(e);
        }

    }
    @FXML
    public void goBack(){
        try {
            stage = (Stage) menuBar.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("user-view.fxml"));
            root = loader.load();

            UserController controller2 = loader.getController();
            controller2.setUser(user);
            controller2.showAdmin(user);
            scene = new Scene(root);
            stage.setScene(scene);

        }catch (Exception e){System.out.println();}
    }
}
