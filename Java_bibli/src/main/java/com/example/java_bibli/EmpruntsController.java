package com.example.java_bibli;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.sql.*;

public class EmpruntsController {

    public User user;
    public Stage stage;
    public Scene scene;
    Parent root;
    @FXML
    TableView table;
    @FXML
    MenuBar menuBar;
    @FXML
    MenuItem empruntsEnCours,empruntsTermine,empruntsTous;

    public void setUser(User user){
        this.user=user;
    }

    public void buildData(int choix){
        ObservableList<Object> data = FXCollections.observableArrayList();
        try{
            table.getItems().clear();
            table.getColumns().clear();
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con= DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque","root","mypassword");
            String sql;
            if(choix==0){
                sql= "SELECT livre.titre, emprunts.debut FROM LIVRE JOIN EMPRUNTS on livre.id=emprunts.livreid JOIN user on user.login=emprunts.userlogin WHERE user.login=? AND emprunts.fin is null";
            }else if(choix==1){
                sql= "SELECT livre.titre, emprunts.debut, emprunts.fin FROM LIVRE JOIN EMPRUNTS on livre.id=emprunts.livreid JOIN user on user.login=emprunts.userlogin WHERE user.login=? AND emprunts.fin is not null ";
            }else{
                sql= "SELECT livre.titre, emprunts.debut, emprunts.fin FROM LIVRE JOIN EMPRUNTS on livre.id=emprunts.livreid JOIN user on user.login=emprunts.userlogin WHERE user.login=? ORDER BY emprunts.fin ASC";
            }
            PreparedStatement stmt = con.prepareStatement(sql);
            String string = String.format("%s", user.getLogin());
            stmt.setString(1,string);
            ResultSet rs = stmt.executeQuery();
            float colCount=rs.getMetaData().getColumnCount();
            /**********************************
             * TABLE COLUMN ADDED DYNAMICALLY *
             **********************************/
            for(int i=0 ; i<rs.getMetaData().getColumnCount(); i++){
                //We are using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i+1));
                col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList,String>,ObservableValue<String>>(){
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList, String> param) {
                        return new SimpleStringProperty(param.getValue().get(j).toString());
                    }
                });

                table.getColumns().addAll(col);
                col.prefWidthProperty().bind(table.widthProperty().multiply(1/colCount));
                System.out.println("Column ["+i+"] ");
            }

            /********************************
             * Data added to ObservableList *
             ********************************/
            while(rs.next()){
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for(int i=1 ; i<=rs.getMetaData().getColumnCount(); i++){
                    //Iterate Column
                    String myValue = rs.getString(i);
                    if (rs.wasNull()) {
                        myValue = "";
                    }
                    row.add(myValue);
                }
                System.out.println("Row [1] added "+row );
                data.add(row);

            }


            //FINALLY ADDED TO TableView
            table.setItems(data);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }

    @FXML
    public void emprunts(ActionEvent event){
        if(event.getSource()==empruntsEnCours){buildData(0);}
        else if(event.getSource()==empruntsTermine){
            buildData(1);}
        else{
            buildData(3);
        }
    }


    @FXML
    public void emprunter(){
        try {
            stage = (Stage) menuBar.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("emprunter.fxml"));
            root = loader.load();

            EmprunterController controller2 = loader.getController();
            controller2.setUser(user);
            controller2.montrerLivres();
            scene = new Scene(root);
            stage.setScene(scene);

        }catch (Exception e){System.out.println();}
    }


    @FXML
    public void goBack(ActionEvent event) throws IOException {

        stage = (Stage) menuBar.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("user-view.fxml"));
        root = loader.load();
        scene = new Scene(root, 960,720);
        stage.setScene(scene);
        UserController controller2 = loader.getController();
        controller2.setUser(user);
        controller2.showAdmin(user);
        stage.show();
        stage.centerOnScreen();
        System.out.println("Deconnexion");
    }

}

