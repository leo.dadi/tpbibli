package com.example.java_bibli;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class AdminMenuController {

    private User user;
    private Stage stage;
    private Parent root;
    private Scene scene;

    @FXML
    TableView table;
    @FXML
    MenuBar menuBar;


    public void setUser(User user) {
        this.user=user;
    }

    public void buildData() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque", "root", "mypassword");
            String sql = "SELECT login FROM USER";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            ObservableList<User> users = FXCollections.observableArrayList();
            double width=0.33;
            TableColumn<User, StringProperty> col1 = new TableColumn<>("ID");
            col1.setCellValueFactory(new PropertyValueFactory<>("login"));
            col1.prefWidthProperty().bind(table.widthProperty().multiply(width));
            TableColumn<User, Button> col2 = new TableColumn<>("Red");
            col2.setCellValueFactory(new PropertyValueFactory<>("red"));
            col2.prefWidthProperty().bind(table.widthProperty().multiply(width));
            TableColumn<User, Button> col3 = new TableColumn<>("Green");
            col3.setCellValueFactory(new PropertyValueFactory<>("green"));
            col3.prefWidthProperty().bind(table.widthProperty().multiply(width));


            table.getColumns().addAll(col1,col2,col3);

            while (rs.next()){
                String login = rs.getString(1);
                users.add( new User(login));
            }

            for(User v:users){
                v.green.setOnAction( e-> v.greenListed());
                v.red.setOnAction(e-> v.redListed());
            }

            table.setItems(users);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    @FXML
    public void goBack(){
        try {
            stage = (Stage) menuBar.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("user-view.fxml"));
            root = loader.load();

            UserController controller2 = loader.getController();
            controller2.setUser(user);
            scene = new Scene(root);
            stage.setScene(scene);

        }catch (Exception e){System.out.println();}
    }
}
