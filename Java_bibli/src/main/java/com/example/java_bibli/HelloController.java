package com.example.java_bibli;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;

public class HelloController {

    Stage stage;
    Scene scene;
    Parent root;
    @FXML
    Label errMessage,welcomeText,userWelcome;
    @FXML
    private Button loginButton;
    @FXML
    TextField nameField;
    @FXML
    private PasswordField passwordField;


    @FXML
    protected void onLoginButtonClick() throws IOException {
        //errMessage.setText("Couple Identifiant/Mot de Passe inconnu \nVeuillez réessayer");
        String name=nameField.getText();
        String password=passwordField.getText();
        System.out.println(name);

        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con=DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Bibliotheque","root","mypassword");
            // con est un objet de type connection
            System.out.println("Tentative de connexion. Recherche login/password");
            String sql = "select * from USER WHERE login = ? and password= ?";
            PreparedStatement prep_stmt = con.prepareStatement(sql);
            prep_stmt.setString(1,name);
            prep_stmt.setString(2,password);
            ResultSet rs = prep_stmt.executeQuery();
            if(rs.next()){
                System.out.println(rs.getString(1));
                User user = new User(rs.getString(1));
                System.out.println(user.getLogin());
                con.close();
                stage = (Stage) loginButton.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(HelloApplication.class.getResource("user-view.fxml"));
                root = loader.load();

                UserController controller2= loader.getController();
                controller2.setWelcomeText(user.getFirstname());
                controller2.setUser(user);
                controller2.showAdmin(user);

                scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
                stage.centerOnScreen();
            }else{
                errMessage.setText("Couple Identifiant/Mot de Passe non reconnu");
            }
        }catch(Exception e){ System.out.println(e);}


        ;

    }

    public Boolean isAdmin(String login) throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection con=DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/Bibliotheque","root","password");
        // con est un objet de type connection
        System.out.println("Tentative de connexion. Recherche login/password");
        String sql = "select admin from USER WHERE login = ?";
        PreparedStatement prep_stmt = con.prepareStatement(sql);
        prep_stmt.setString(1,login);
        ResultSet rs = prep_stmt.executeQuery();
        con.close();
        return rs.getBoolean(3);

    }



}